# Qubla / Qurator Compatible R Components

A few scripts and yaml files that will work in the Qurius stack.



## Loading Requirements

R has no built-in utility for checking for a library and downloading it if it's not present.  Don't ask me, I didn't make
the rules.  So we include a function at the top of the scripts to do just this.  The good thing is, the library 
will only be installed locally.  You should be able to copy and paste the code below into your script.

```
loadDep <- function(x) {
  if(!(x %in% installed.packages())){
    print("need to install package")
    install.packages(x)
  }
  require(x, character.only=TRUE)
}
```

Now add your libraries using the function below instead of the ```library(Xmisc)``` command

```
loadDep("Xmisc")
loadDep("hdfs")
```

## Packrat

### One-Time Global Installation

To isolate dependencies, e.g. so you don't have to do ```install.packages("mypackage")``` globally, we encourage
the use of [packrat](https://rstudio.github.io/packrat/).  Ironically, you'll have to install packrat globally to
use it.  Again, I didn't make the rules.

Open your R terminal and run

```
install.packages("packrat")
```

and it will exist globally.  Of course, you could have used the ```loadDep``` function above, but it's not necessary. 

### Using PackRat in New Projects

You should walk through the [packrat walkthrough](https://rstudio.github.io/packrat/walkthrough.html). The projects here
in this repository have already been set up so you don't have to run ```packrat::init()```.  If you did want to create
your own new project, the walkthrough boils down to

```
# create a project directory
> mkdir humphrey-bogart
> cd humphrey-bogart
# Start R
> R
R:> packrat::init()
R:> q('no')
```

and place any R script (like "humphrey.R") you like in this directory, at the top. The directory should look like

```
> pwd
~/humphrey-bogart
> ls -alh
drwxr-xr-x   6 buddha  staff   204B Nov  5 11:55 .
drwxr-xr-x  16 buddha  staff   544B Nov  5 11:52 ..
-rw-r--r--   1 buddha  staff   115B Nov  5 11:53 .Rprofile
-rw-r--r--   1 buddha  staff   193B Nov  5 11:55 humphrey.R
drwxr-xr-x   9 buddha  staff   306B Nov  5 11:54 packrat
```

So you have an ```.Rprofile``` file and and a packrat directory. I went ahead and added a few lines to humphrey:

```
> cat humphrey.R
source(".Rprofile")


loadDep <- function(x) {
  if(!(x %in% installed.packages())){
    print(c("need to install package ", x))
    install.packages(x,repos="http://cran.stat.ucla.edu/")
  }
  require(x, character.only=TRUE)
}
```

Put the rest of your regular R stuff below that. If you load packages now, they are loaded locally, not globally, so you
don't have to worry about conflicts!

## Working with source control like GitLab

When you run ```install.packages()``` you are putting a blob (binary large object) in a local directory. It's not cool to
check blobs into source control (like GitLab or SVN) so we want git to ignore them.

# Arguments

Let's be nice to the next guy (who might be you) and add some arguments and a help line.  Here is an
[R Package](https://cran.r-project.org/web/packages/Xmisc/vignettes/Xmisc-ArgumentParser.pdf) that is very helpful, but
alas, poorly documented.


# Data

Some of these scripts use Twitter Data.  I'm not allowed to share it here, so you'll have to get your won. I do have some intermediate product data in the data/folder, however.


