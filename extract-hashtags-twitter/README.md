This R script transforms Twitter JSON output to a structured CSV format.

From the command line, use

```
Rscript extractHashTags.R --input /path/to/json/twitter/file --output  /path/to/output/csv
```

It's using the [jsonlite](https://cran.r-project.org/web/packages/jsonlite/jsonlite.pdf) library.
